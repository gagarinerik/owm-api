package openWeatherMap;

import io.restassured.response.Response;

import static io.restassured.RestAssured.given;

public class OpenWeatherMapApi {

	private static final String APPID = "2a8a5531bd0176d688284928b5dacc8d";
	private static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";
	private static final String CURRENT_WEATHER_API = "weather";
	private static final String WEATHER_FORECAST_API = "forecast";
	private static final String UNITS = "metric";

	public static Response getCurrentWeatherByCityName(String cityName) {
		return given()
				.queryParam("q", cityName)
				.queryParam("appid", APPID)
				.queryParam("units", UNITS)
				.when()
				.get(BASE_URL + CURRENT_WEATHER_API);
	}

	public static Response getWeatherForecastByCityName(String cityName) {
		return given()
				.queryParam("q", cityName)
				.queryParam("appid", APPID)
				.queryParam("units", UNITS)
				.when()
				.get(BASE_URL + WEATHER_FORECAST_API);
	}
}